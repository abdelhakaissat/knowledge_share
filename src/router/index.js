import { createRouter, createWebHistory } from 'vue-router';


import Home from '../views/Home.vue';
import About from '../views/About.vue';
import Courses from '../views/Courses.vue';
import AllCourses from '../views/AllCourses.vue';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/about',
        name: 'About',
        component: About,
    },
    {
        path: '/courses',
        name: 'Courses',
        component: Courses,
    },
    {
        path: '/all-courses',
        name: 'AllCourses',
        component: AllCourses,
    }
];
const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;