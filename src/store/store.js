import { defineStore } from 'pinia'

export default defineStore('store', {
  state: () => {
    return { 
        // initial state
        loading: false,
        courses: [],
     }
  },
  actions: {
    setIsLoading(val) {
      this.loading = val;
    },
    setCourses(courses) {
        this.courses = courses;
    },
  },
})

/*
export const useAppStore = defineStore('app-store', {
  state: () => {
    return { 
        // initial state
        loading: false;
        courses: [],
     }
  },
  actions: {
    setIsLoading(val) {
      this.loading = val;
    },
    setCourses(courses) {
        this.courses = courses;
    },
  },
})
*/


// then in  vue component
// import {useAppStore} from ...

// let's say we want to save the courses
// const store = useAppStore();
// store.setCourses([.......])